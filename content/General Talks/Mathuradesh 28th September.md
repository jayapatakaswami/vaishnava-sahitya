---
title: "Zoom Address 28th September"
date: 2020-09-30T12:53:55-04:00
draft: false
---


**20200828 JAYAPATĀKĀ SWAMI GURU MAHARAJ, ZOOM ADDRESS TO MATHURADESH
DEVOTEES, ŚRĪ MĀYĀPUR DHAM, INDIA**

Thank you very much for sharing the report with me. I am glad to see
that many devotees are engaging in outreach, even though there is health
issues and other difficulties. We pray that Krsna will bless everyone!
We have one doctor here who has some alternative therapies and is up to
date on the latest allopathic medicines, he is an MD from America. He is
used to tele medicine through video consultation. If you need he can
also come there and see you online and help you. There are some
alternative things that help. Like the Russian patch therapy which can
help one to avoid getting this COVID-19 infection. Those who are taking
allopathic medicines can also do alternative treatments. And they can
get cured more quickly.

We have to very quick home visits. Switching from one house to the next
every 15 seconds.

mūkaṁ karoti vācālaṁ paṅguṁ laṅghayate girim

yat-kṛpā tam ahaṁ vande śrī-guruṁ dīna-tāraṇam

paramānandaṁ mādhavaṁ śrī caitanya iśvaram

hariḥ oṁ tat sat

So is my voice understandable or you also need repetition. So I will
speak in English and he will translate in Hindi. Is that alright? We
know that the world is right now under extreme difficulty. There is
corona virus pandemic, some places there is cyclones, hurricanes, some
places floods, some places forest fires, and like this there are many
difficulties, some places wars. Not far from you in Yemen and Saudi
Arabia there is some situation. So Srila Prabhupada has said that these
natural calamities, pandemic, war, floods etc. are due to our breaking
the laws of God, the laws of nature. And this is bad that people may
understand that this is not a place for us to stay. So the devotees by
chanting Hare Krsna, by chanting the name of God, they can go back to
the spiritual world. In different scriptures of the world, they glorify
the name of the Lord. So, whichever name you have faith in -- some bona
fide name of God, should be chanted. We have names in Sanskrit, names of
the Lord. We have faith in those. Like, Krsna, Govinda, Narayana, Visnu.
The Lord and His names are not different. Previously, people are more
pious in Satya they did meditation, Treta they did homa yagna, in
Dvapara they did temple worship. But now the people in Kali yuga are
most fallen. So the Lord, He came as His holy name. So that people could
chant the holy name and achieve all success. And there is no hard fast
rule for chanting the name. Whether one chants attentively with
devotion, whether one chants it accidentally or meaning something.
Ajamila, he was calling his son, he gave his son the name Narayana. He
kept calling him Narayana, come here! Narayana walk with me! Narayana
eat your meal! So like this he was chanting the name Narayana. So when
the Yamadutas came at his death, he also called his son. Narayana!
NARAYANA! And then he remembered the original Narayana. The Visnudutas
came and saved him. So this name of the Lord is so powerful that it can
solve all the problems of the Kali yuga. So that is why we are trying to
encourage all the people to chant the holy names. Even if people chant
one round, they can gradually see a change in their consciousness. If
anyone chants 16 rounds, then they can see extreme benefit. So, chanting
the holy names is recommended in this Kali yuga.

In the Srimad Bhagavatam it states that we should not make our goal in
life sense gratification. We are spirit souls; we have material bodies.
Bodies need a certain amount of sense gratification. So, at the same
time we should make our goal to serve Krsna, to love Krsna. And dharma
viruddho bhuteshu kaamo asmi. Krsna says that He is the sex life which
is not against the scriptures. We can see that the same senses sometimes
give us happiness and sometimes sorrow. So, we want to focus our
attention on pleasing Krsna. Our goal in life is Lord Krsna. Meanwhile
we may have sometimes pleasure from the senses and sometimes suffering.
This material world is a place of suffering. Anyway also some people are
giving us short videos for the bhakti vrksa modules. They are very
interesting. I am still waiting for the videos from Mathurdesh! If you
have made any sort videos? I don't know if you have any video I will see
you. (We will send one sample to Braja Prana prabhu to make the video).
We are very happy that you are using your energy to spread Krsna
consciousness preaching. This way everyone can perfect their lives.
Those who are going back to India, we have been discussing that please
tell us when you go and we will try to engage them. Association in the
gulf is very friendly, very nice and something that devotees miss when
they go back to India. So we will try to make their experience as
favorable as possible. And we were discussing that we should welcome the
devotees, who are coming from the Middle East. You were saying that some
devotees lost their jobs. But if they go back to India, please let us
know. We will try to engage them in some service. In India rules are a
little different than in the Middle East. So it will take some time for
them to adapt. I have great interest to facilitate the devotees that
come back. In Mayapur we have many devotees who are either MAC members
or divsional heads. So we hope that devotees who come back from the
Middle East, they will have a lot of opportunities for service. Anyway,
since we have home visits, I will not speak too much. One thing is that
we have the Jayapataka Swami app made for Android and IOS. And I put in
messages. Radhastami I sent 21 messages! But at least four or five
messages every day, I send. Devotees have seen that there is a new app
but have not downloaded. It is also in about ten different languages. So
it is a great opportunity and we hope everyone downloads the app, and
give us feedback how you liked the app. Did you download it? I am very
happy to see that how husband and wife together are serving Lord Krsna
in devotional service. It is very exemplary. Srila Prabhupada said that
there is no reason to think about taking sannyasa if your wife is
cooperating well in devotional service. Of course, in ISKCON unless the
wife gives a written permission, no one is given sannyasa. Since I am
observing my 50^th^ anniversary of sannyasa, maybe some devotees are a
bit scared that it is 50 years. But nothing to worry about. You just
unitedly serve the Lord. And I hope that the husband and wife treat each
other very nice. They are vaisnava and vaisnavi, so we shouldn't commit
any offence or aparadha. Haribol!

Transcribed by Jayaraseshwari devī dāsī,

30 September 2020