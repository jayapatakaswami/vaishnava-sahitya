---
title: "27th September 2020"
date: 2020-09-27T12:53:55-04:00
draft: false
weight: 20200927
---


### By His Holiness Jayapatākā Swami Mahārāja on 27th September 2020 in Śrīdhāma Māyāpur, India


**Śrī Kṛṣṇa Caitanya Book Compilation**

**By His Holiness Jayapatākā Swami Mahārāja on 27th September 2020 in Śrīdhāma Māyāpur, India**

{{< youtube id="AGlBSIO9ZlE" autoplay="false" >}}


govardhanadharam vande  
gopālam goparūpiṇam  
gokulotsavam īśānam  
govindam gopikāpriyam  



mūkaṁ karoti vācālaṁ paṅguṁ laṅghayate girim  
yat-kṛpā tam ahaṁ vande śrī-guruṁ dīna-tāraṇam  
paramānandaṁ mādhavaṁ śrī caitanya iśvaram 
 hariḥ oṁ tat sat


**Introduction:** We are continuing with Caitanya-līlā compilation **,** Today Chapter entitled as **:**

**Nityānanda Prabhu Informs Śacīmātā and the Devotees in Māyāpur to Come to Śāntipura**

**Caitanya-bhāgavata Antya-khaṇḍa 1.144**

prabhu-nityānandera śrīdhāma māyāpure āgamana  
ei mata gaṅgā-madhye bhāsiyā bhāsiyā  
navadvīpe prabhu-ghāṭe uṭhila āsiyā

**Jayapatākā Swami:** In this way while floating in the Ganges, Nityānanda Prabhu finally arrived at the Mahāprabhu's ghāṭa in Navadvīpa and then he rose up

**Purport by Śrīla Bhaktisiddhānta Sarasvatī Ṭhākura:** From the western bank of the Ganges known as Kuliyā, Śrī Nityānanda Prabhu floated to Mahāprabhu's ghāṭa on the eastern bank of the Ganges.

**Murāri Gupta Kaḍaca 3.4.7:** Then Nityānanda entered auspicious āśrama of Śrīvāsa Ṭhākura and reported the instruction of Gaura Keśava to Śrīvāsa and the other bhaktas.

**Caitanya-bhāgavata Antya-khaṇḍa 1.145**

āpanā&#39; samvari&#39; nityānanda-
 prathame uṭhilā āsi&#39; prabhura ālaya

**Jayapatākā Swami:** Lord Nityānanda Mahāśaya controlling Himself, once after arriving He went straight to Lord Gaurāṅga&#39;s house.

**Caitanya-bhāgavata Antya-khaṇḍa 1.146**

abhinna-vrajendranandana gaurasundarera virahe abhinna-yaśomati śacīdevīra kṛṣṇa-virahoddīpana—
 āsiyā dekhaye āi dvādaśa-upavāsa
 sabe kṛṣṇa-bhakti-bale dehe āche śvāsa

**Jayapatākā Swami:** When He arrived, He found that Mother Śacī had been fasting for twelve days. She had been surviving simply by the strength of her devotional service to Lord Kṛṣṇa.

**Purport by Śrīla Bhaktisiddhānta Sarasvatī Ṭhākura:** Śrī Gaurasundara passed twelve days going from Māyāpur to Katwa to take sannyāsa and wandering in Rāḍha-deśa. Mother Śacī abstained from taking any food or drink for those twelve days.

**Jayapatākā Swami:** It was such an intense feeling of separation from Lord Caitanya. Mother Śacī was not eating for twelve days

**Caitanya-bhāgavata Antya-khaṇḍa 1.147**

yaśodāra bhāve āi parama-vihvala
 niravadhi nayane vahaye prema-jala

**Jayapatākā Swami:** Mother Śacī was transcendentally overwhelmed in the mood of Mother Yaśodā. She Mother Śacī constantly shed tears of love from her eyes.

**Caitanya-bhāgavata Antya-khaṇḍa 1.148**

yāre dekhe āi tāhārei vārtā kaya
 &quot;mathurāra loka ki tomarā saba haya?

**Jayapatākā Swami:** Mother Śacī asked whomever she met, &quot;She would see, &quot;Are you from Mathurā? Do you have any news of Kṛṣṇa?&quot;

**Purport by Śrīla Bhaktisiddhānta Sarasvatī Ṭhākura:** Feeling separation from Śrī Gaurasundara, Mother Śacī would ask everyone, &quot;Are you people from Mathurā? Do you have any news about Kṛṣṇa and Balarāma?&quot; She was feeling anxiety thinking about the visit of Akrūra, and she would hear the sound of Kṛṣṇa&#39;s flute and Balarāma&#39;s horn.

**Jayapatākā Swami:** Mother Śacī was absorbed in the mood of Yaśodā who was feeling great separation from Kṛṣṇa and Balarāma, when Akrūra took them to Mathurā. We see that Lord Caitanya is more merciful than Kṛṣṇa. Kṛṣṇa didn&#39;t come back to Vṛndāvana but Lord Caitanya saw all Navadvīpa-vāsīs in Śāntipur.

**Caitanya-bhāgavata Antya-khaṇḍa 1.149**

kaha kaha rāma-kṛṣṇa āchaye kemane?&quot;
 baliyā mūrcchita hañā paḍilā takhane

**Jayapatākā Swami:**&quot;Please tell me, how are Rāma, Kṛṣṇa and Balarāma?&quot; Speaking like this, she would fall unconscious to the ground. So, we see here Mother Śacī was absorbed in the mood of Mother Yaśodā and was mad in ecstasy and she would ask everyone where is Kṛṣṇa and Balarāma although Her son is Śacīsuta.

**Caitanya-bhāgavata Antya-khaṇḍa 1.150**

kṣaṇe bale āi,—&quot;oi veṇu śiṅgā bāje
 akrūra āilā kibā punaḥ goṣṭha mājhe?&quot;

**Jayapatākā Swami:** Sometimes Mother Śacī would say, &quot;Is that the sound of the flute and horn? Has Akrūra returned to Vṛndāvana?&quot;

**Purport by Śrīla Bhaktisiddhānta Sarasvatī Ṭhākura:** In the Śrīmad-Bhāgavatam (10.46.18-19) it is stated:

&quot;Does Kṛṣṇa remember us? Does He remember His mother and His friends and well-wishers? Does He remember the cowherds and their village of Vraja, of which He is the master? Does He remember the cows, Vṛndāvana forest, and Govardhana Hill?

&quot;Will Govinda return even once to see His family? If He ever does, we may then glance upon His beautiful face, with its beautiful eyes, nose, and smile.&quot; One should also see Śrīmad-Bhāgavatam, Tenth Canto, Chapters 38-39.

**Jayapatākā Swami:** So, this was the mood of Mother Yaśodā, somehow Mother Śacī was feeling the same mood. She was feeling separation from Kṛṣṇa.

**Caitanya-bhāgavata Antya-khaṇḍa 1.151**

ei mata āi kṛṣṇa-viraha-sāgare
 ḍubiyā āchena bāhya nāhika śarīre

**Jayapatākā Swami:** In this way Mother Śacī, she drowned in an ocean of separation from Kṛṣṇa. She lost all external consciousness.

**Caitanya-bhāgavata Antya-khaṇḍa 1.152**

śacīdevī-samīpe nityānandera āgamana—
 nityānanda prabhu-vara henai samaya
 āira caraṇe āsi&#39; daṇḍavat haya

**Jayapatākā Swami:** At that time Lord Nityānanda came there and fell flat offering His prostrated obeisances at the Mother Śacī&#39;s lotus feet

**Caitanya-bhāgavata Antya-khaṇḍa 1.153**

nityānande dekhi&#39; saba bhāgavata-gaṇa
 uccaiḥ-svare lāgilena karite krandana

**Jayapatākā Swami:** When all the devotees saw Lord Nityānanda, they began to cry very loudly.

**Caitanya-bhāgavata Antya-khaṇḍa 1.154**

&quot;bāpa bāpa,&quot; bali āi hailā mūrcchita
 nā jāniye ke vā paḍaye kon bhita

**Jayapatākā Swami:** Mother Śacī called out &quot;My son! My son!&quot; and She fell, lost consciousness at that time all the devotees were also fell unconscious no one could say who fell in which direction.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.56**

śacī ati unamati dhāya ūrddhamukhe
 e bhumi-ākāśa śacīra juḍileka duḥkhe

**Jayapatākā Swami:** Mother Śacī wildly ran with head up looking towards the sky. It seemed like Mother Śacī in her grief the earth and sky become one.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.57**

ārttanāde ḍāke śacī–āre avadhūta
 kothā thuñā āli mora nimāi soṇāra suta

**Jayapatākā Swami:** In a voice filled with grief Śacī called out, &quot;Āre! Oh! Avadhūta!&quot; in front of Nityānanda, She said, &quot;Where did you keep my golden son Nimāi?&quot;

**Caitanya Maṅgala, Madhya-khaṇḍa 14.58**

ihā bali&#39; kānde śacī buke kara hāne
 ṭalamala kare,–nāhi cāhe pathapāne

**Jayapatākā Swami:** Speaking in this way, Mother Śacī beat her chest with her hand. She trembled and wobbled. She could not even see the pathway.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.59**

śacī dekhi&#39; abhyutthāna karilā ṭhākura
 śacī kahe–mora putra āise katadura

**Jayapatākā Swami:** Seeing Mother Śacī, Lord Nityānanda stood respectfully. Mother Śacī said, &quot;How far away is my son?&quot;

**Murāri Gupta Kaḍaca 3.4.8:** Then Śrī Nityānanda, the ocean of mercy, bowed with folded hands to the lotus feet of Śrī Śacī Devī, and through His devotion, gave peace to her mind.

**Caitanya-bhāgavata Antya-khaṇḍa 1.155**

nityānanda prabhu-vara sabā&#39; kari&#39; kole
 siñcilena sabāra śarīra prema-jale

**Jayapatākā Swami:** Nityānanda Prabhu embraced all the devotees and soaked their bodies with tears of transcendental love of Kṛṣṇa.

**Caitanya-bhāgavata Antya-khaṇḍa 1.156**

śrīnityānanda-kartṛka mahāprabhura śāntipure āgamana-vārtā-jñāpana—
 śubha-vāṇī nityānanda kahena sabāre
 &quot;satvare calaha sabe prabhu dekhibāre

**Jayapatākā Swami:** Lord Nityānanda informed everyone of the auspicious news and said, &quot;Let us quickly go to see the Lord Gaurāṅga.&quot;

**Caitanya-bhāgavata Antya-khaṇḍa 1.157**

śāntipura gelā prabhu ācāryera ghare
 āmi āilāṅa tomā&#39;-sabā laibāre&quot;

**Jayapatākā Swami:**&quot;The Lord Viśvambhara had gone to Śāntipura to Advaita Ācārya&#39;s house. I have come here to take all of you there.&quot;

**Caitanya Maṅgala, Madhya-khaṇḍa 14.60**

nityānanda kahe–kheda nā kariha cite
 āmāre pāṭhāilā tomā-sabhākāre nite

**Jayapatākā Swami:** Lord Nityānanda said, &quot;Please do not be unhappy at heart. He sent Me to fetch all of you.&quot;

**Caitanya Maṅgala, Madhya-khaṇḍa 14.61**

advaita-ācārya-ghare rahiba ṭhākura
 kheda nā kariha–dekhā haiba adūra

**Jayapatākā Swami:**&quot;The Lord Gaurāṅga will stay at Advaita Ācārya&#39;s house. Do not lament, You will see the Lord very quickly.&quot;

**Caitanya-bhāgavata Antya-khaṇḍa 1.158**

caitanya-virahe jīrṇa sarva bhakta-gaṇa
 pūrṇa hailāśuni&#39; nityānandera vacana

**Jayapatākā Swami:** All the devotees had become morose with feelings of separation from Lord Caitanya, but when they heard Lord Nityānanda&#39;s words, they became joyful.

**Purport by Śrīla Bhaktisiddhānta Sarasvatī Ṭhākura:** According to the Amara-kośa dictionary: pravayāḥ sthaviro vṛddho jīno jīrṇo jarannapi—&quot;Pravayāḥ, sthavira, vṛddha, jīna, and jīrṇa are all synonyms for an old person.&quot; Also, according to the Amara-kośa dictionary: samagraṁ sakalaṁ pūrṇam akhaṇḍaṁ syādanūnake. pūrṇas tu pūrite—&quot;Samagra, sakala, pūrṇa, and akhaṇḍa all mean &#39;not deficient,&#39; and pūrṇa means &#39;full.&#39;&quot;

**Jayapatākā Swami:** So, devotees in Navadvīpa due to great separation from Lord Caitanya, were feeling like wasted like old and worn. when they heard the news from Lord Nityānanda, they become full and enlivened.

**Caitanya-bhāgavata Antya-khaṇḍa 1.159**

sabei hailā ati ānande vihvala
 uṭhila paramānanda kṛṣṇa-kolāhala

**Jayapatākā Swami:** Everyone became overwhelmed with extreme ecstasy, and arouse a transcendental ecstasy and tumultuous vibration of Lord Kṛṣṇa&#39;s holy names.

**Caitanya-bhāgavata Antya-khaṇḍa 1.160**

upabāsinī śacīdevī—
 ye divasa gelā prabhu karite sannyāsa
 se divasa haite āira upavāsa

**Jayapatākā Swami:** From the day the Lord Caitanya left to take sannyāsa, from that day Mother Śacī had fasted.

**Caitanya-bhāgavata Antya-khaṇḍa 1.161**

dvādaśa-upāsa tāna—nāhika bhojana
 caitanya-prabhāve mātra āchaye jīvana

**Jayapatākā Swami:** She had fasted completely for twelve days. Surviving simply by the influence of Lord Caitanya was she able to survive.

We see that Lord Nityananda coming had brought life back into bodies of devotees of Navadvīpa, they were ecstatic that they would soon see Lord Caitanya. They were chanting the holy names Hari Bol! Hari Bol! Gaurāṅga! Nityānanda! Gaurāṅga Nityānanda!

Hare Kṛṣṇa, Hare Kṛṣṇa, Kṛṣṇa Kṛṣṇa, Hare Hare/
 Hare Rāma, Hare Rāma, Rāma Rāma, Hare Hare

Thus Ends the Chapter, Nityānanda Prabhu Informs Śacīmātā and the Devotees in Māyāpur to Come to Śāntipura

Transcribed by Jayarāseśvarī devī dāsī - 27th September 2020

Verified by JPS Archives team - 27th September 2020