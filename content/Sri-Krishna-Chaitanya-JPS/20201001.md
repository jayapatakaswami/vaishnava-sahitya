---
title: "1st October 2020"
date: 2020-09-29T12:53:55-04:00
draft: false
weight: 20201001
---


**Śrī Kṛṣṇa Caitanya Book Compilation**

**By His Holiness Jayapatākā Swami Mahārāja on 1st October 2020 in
Śrīdhāma Māyāpur, India**

{{< youtube id="z7BXQ-8b6gA" autoplay="false" >}}


*govardhanadharam vande\
gopālam goparūpiṇam\
gokulotsavam īśānam\
govindam gopikāpriyam*

*mūkaṁ karoti vācālaṁ paṅguṁ laṅghayate girim\
yat-kṛpā tam ahaṁ vande śrī-guruṁ dīna-tāraṇam\
paramānandaṁ mādhavaṁ śrī caitanya iśvaram\
hariḥ oṁ tat sat*

**Introduction:** Today's topic:

**[Lord Caitanya Reaches the House of Advaita Ācārya]{.ul}**

**Caitanya Candrodaya Nāṭaka 5.33:** Bhagavān: (enacts happiness)
Śrīpāda, where is Yamunā?

**Caitanya Candrodaya Nāṭaka 5.34:** Nityānanda: This way. This way. (He
brings Him a certain distance. They reach the Gaṅgā). Lord, this is
Yamunā.

**Caitanya-caritāmṛta, Madhya-līlā 3.25**

*gaṅgāke 'yamunā baliyā pradarśana :---\
prabhu kahe, --- kata dūre āche vṛndāvana\
teṅho kahena, --- kara ei yamunā daraśana*

**Translation:** When the Lord asked Nityānanda Prabhu how far it was to
Vṛndāvana, Nityānanda replied, "Just see! Here is the river Yamunā."

**Caitanya-caritāmṛta, Madhya-līlā 3.26**

*mahāprabhura gaṅgādarśane yamunā-uddīpana\
eta bali' ānila tāṅre gaṅgā-sannidhāne\
āveśe prabhura haila gaṅgāre yamunā-jñāne*

**Translation:** Saying this, Nityānanda Prabhu took Caitanya Mahāprabhu
near the Ganges, and the Lord, in His ecstasy, accepted the river Ganges
as the river Yamunā.

**Caitanya-caritāmṛta, Madhya-līlā 3.27**

*prabhura yamunā-stava :---\
aho bhāgya, yamunāre pāiluṅ daraśana\
eta bali' yamunāra karena stavana*

**Translation:** The Lord said, "Oh, what good fortune! Now I have seen
the river Yamunā." Thus thinking the Ganges to be the river Yamunā,
Caitanya Mahāprabhu began to offer prayers to it.

**Caitanya-caritāmṛta, Madhya-līlā 3.28**

*Caitanya-candrodaya-nāṭaka (5/13) - dhṛta pādmavākya---\
cid-ānanda-bhānoḥ sadā nanda-sūnoḥ\
para-prema-pātrī drava-brahma-gātrī\
aghānāṁ lavitrī jagat-kṣema-dhātrī\
pavitrī-kriyān no vapur mitra-putrī*

**Translation:** "O river Yamunā, you are the blissful spiritual water
that gives love to the son of Nanda Mahārāja. You are the same as the
water of the spiritual world, for you can vanquish all our offenses and
the sinful reactions incurred in life. You are the creator of all
auspicious things for the world. O daughter of the sun-god, kindly
purify us by your pious activities."

**Purport by Śrīla Prabhupāda:** This verse is recorded in the
*Caitanya-candrodaya-nāṭaka* (5.13), by Kavi-karṇapūra.

**Caitanya Candrodaya Nāṭaka 5.35: Bhagavān:** (happily bowing down,
recites:) "O river Yamunā, you are the blissful spiritual water that
gives love to the son of Nanda Mahārāja. You are the same as the water
of the spiritual world, for you can vanquish all our offenses and the
sinful reactions incurred in life. You are the creator of all auspicious
things for the world. O daughter of the sun-god, kindly purify us by
your pious activities."

**Caitanya Candrodaya Nāṭaka 5.36: Nityānanda:** Lord, now You should
bathe in the Yamunā.

**Caitanya Candrodaya Nāṭaka 5.37: Bhagavān:** As it pleases You. (He
enacts bathing).

**Caitanya-caritāmṛta, Madhya-līlā 3.29**

*eka kaupīnamātra sambala prabhu :---\
eta bali' namaskari' kaila gaṅgā-snāna\
eka kaupīna, nāhi dvitīya paridhāna*

**Translation:** After reciting this mantra, Śrī Caitanya Mahāprabhu
offered obeisances and took His bath in the Ganges. At that time He had
on only one piece of underwear, for there was no second garment.

**Caitanya Candrodaya Nāṭaka 5.38: Nityānanda:** (aside) Now I am
relieved. The greatly-maddened jungle elephant is controlled merely by a
mantra. Now I will carry out whatever remaining activities are to be
accomplished. (He looks in all directions, and then calls out to
someone).

**Jayapatākā Swami:** Lord Nityānanda had brought Lord Caitanya to the
Ganges. Actually the Ganges, Gaṅgā Yamunā and Sarasvatī meet together in
Prayāga. There the Yamunā is in the western bank of the Ganges and the
Gaṅgā is on the eastern side and Sarasvatī in the middle so practically
He didn't lie He took Lord Caitanya to the Yamunā. Since Yamunā is on
the western bank so although in this point He is calling it as Gaṅgā we
could also say that its western bank is Yamunā.

**Caitanya Candrodaya Nāṭaka 5.39:** (A man enters and bows down to
offer respects).

**Caitanya Candrodaya Nāṭaka 5.40: Nityānanda:** (whispering aside to
the man) Aye (a vocative particle an interjection)! Not far from here,
just on the opposite shore of the Gaṅgā is Lord Advaita\'s home. Please
quickly go there and inform: \"Nityānanda is waiting nearby with another
*sannyāsī* expecting your arrival. So, come quickly.\"

**Caitanya Candrodaya Nāṭaka 5.41: The Man:** I will do thus. (He
quickly exits).

**Jayapatākā Swami:** *Śrī Caitanya Candrodaya Nāṭaka* is a special
drama of Lord Caitanya pastimes. we include in this compilation because
it gives us some details about the pastimes, which are not explained
elsewhere in the same way.

**Caitanya Candrodaya Nāṭaka 5.42: Nityānanda:** (to himself) Oh! Today
three days have passed and since then I have not even touched water. So,
I will also bathe. (Thus, He does that).

**Caitanya Maṅgala, Madhya-khaṇḍa 14.65:**

*advaita-ācārya-ghare uttarila giyā\
bhāṅgila kāṁkāli tāṁhā prabhu nā dekhiyā*

**Jayapatākā Swami:** From Navadvīpa devotees have reached Advaita
Ācārya\'s house. They felt broken hearted not being able to see Lord
Gaurāṅga.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.66**

*advaita-ācārye kathā puche nityānanda\
tomāra āśrame prabhu karilā nirbandha*

**Jayapatākā Swami:** Lord Nityānanda asked Advaita Ācārya: \"Lord
Gaurāṅga said He would come to Your *āśrama*."

**Caitanya Maṅgala, Madhya-khaṇḍa 14.67**

*āmāre pāṭhāñā dila e sabhāre nite\
āra kichu nā jāniye ki āchaye cite*

**Jayapatākā Swami:** "He sent Me to bring everyone. What was in His
heart that He didn't tell me?"

**Caitanya Maṅgala, Madhya-khaṇḍa 14.68**

*ihā bali' doṁhe meli' kare kolākuli\
gaurāṅga-sannyāsa śuni' advaita vikalā*

**Jayapatākā Swami:** After Lord Nityānanda spoke these words the two of
Them embraced. Hearing that Lord Gaurāṅga had accepted sannyāsa, Lord
Advaita was distraught at heart.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.69**

*muñi abhāgiyā saṅga nā pāila tāra\
kabe cāṁdamukha mo dekhi ārabāra*

**Jayapatākā Swami:** Advaita Gosvāmī said: \"I am very unfortunate,
that I did not have His association. When will I see His moon-like face
again?\"

**Caitanya Maṅgala, Madhya-khaṇḍa 14.70**

*śacī unamati puche takhani takhana\
sabajana bole--prabhu āsiba ekhana*

**Jayapatākā Swami:** Mother Śacī was filled with grief, She inquired
about her son, everyone said to her: \"Lord Caitanya will come now.\"

**Caitanya Maṅgala, Madhya-khaṇḍa 14.71**

*utkaṇṭhā bāḍhila sabajanāra hṛdaya\
āilā ta mahāprabhu henai samaya*

**Jayapatākā Swami:** Everyone's heart longing increased. At that moment
Lord Gaurāṅga Mahāprabhu arrived.

**Caitanya Candrodaya Nāṭaka 5.43: A Voice from Behind the Scenes:** Our
life-breath cannot depart although tortured by separation from Him
because of being tightly bound by the rope of hope of attaining the Lord
and kind, amiable qualities He displayed towards us devotees. Ah!
(Previously holding the life air was unbearable due to being separated
from the Lord, but) At present, these life airs have benefitted us in
seeing the Lord's face. Though unfavorable, due to fate, suddenly it has
become favorable.

**Caitanya Candrodaya Nāṭaka 5.44: Nityānanda:** (listening from a
distance) Ah! This is Advaita Ācārya. reciting prayers. This is well
arranged by destiny. Hereafter, my burden has been reduced (by obtaining
the help of Advaita Ācārya). (Seeing the Lord). Ah! What a suffering!

**Caitanya Candrodaya Nāṭaka 5.45:** Gaurāṅga, placing his two hands on
His head, came out of Ganges and is standing on the banks. Streams of
water is wetting His body, but due to not having practiced (thinking
that the *sannyāsīs* don't dry the *kaupīna* by squeezing the water
after bathing) is wearing the wet *kaupīna* and the outer cloth without
removing the water by squeezing. He has no external sense is not ashamed
(of wearing the wet clothes) because of being overwhelmed by love. His
coming out of the water appears to be just like elephants come out of
the water holding on the head read lotus (The hand on His head appears
like a lotus). I see this golden Lord.

**Jayapatākā Swami:** So we see how Lord Caitanya is so beautiful, that
elelphant may hold a lotus Lord Caitanya hands appears more beautiful
than the lotus. The devotees are filled with ecstatic happiness to see
the Lord.

**Caitanya Candrodaya Nāṭaka 5.46:** I see there is some delay in
Advaita\'s coming here. (Having said this, He looks ahead in front).

**Caitanya Candrodaya Nāṭaka 5.47:** (Accompanied by countless
associates, Advaita eagerly enters).

**Caitanya Candrodaya Nāṭaka 5.48: Advaita:** (looking ahead) Aho! He
is, He is the Lord. From a distance, because of His shaved head and
saffron garment makes Him look like not the Lord (looks like another
person). Still He appears to us with the golden bodily luster and
exceptional handsomeness. What a wonder!

**Jayapatākā Swami:** They are seeing Lord Caitanya as a *sannyāsī* with
shaved head for the first time and wearing saffron cloth, but they can
see that he appears wonderful.

**Caitanya Candrodaya Nāṭaka 5.49:** All of you look at the Lord\'s
form, as yellow as gold, and dressed in a red (saffron) garment. His
opulence of *vairāgya* is like a golden-yellowish ripened mango fruit
abundant with juice.

(He runs up to the Lord, whose eyes are closed in meditation. Standing
before the Lord, Advaita cries without restraint).

**Caitanya-caritāmṛta, Madhya-līlā 3.30**

*advaitācāryera āgamana :---\
hena kāle ācārya-gosāñi naukāte caḍiñā\
āila nūtana kaupīna-bahirvāsa lañā*

**Translation:** While Śrī Caitanya Mahāprabhu was standing there
without a second garment, Śrī Advaita Ācārya arrived in a boat, bringing
with Him new underwear and external garments.

**Jayapatākā Swami:** It seems like Lord Nityānanda was simultaneously
at two places at once, which is not wonderful for Lord Nityānanda.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.72**

*āchila-adhika koṭiguṇa deha-chaṭā āra tāhe\
āila nūtana kaupīna bahirvāsa lañā*

**Jayapatākā Swami:** His transcendental body was ten million times more
effulgent. He was anointed with brilliant long *tilaka* marks of
sandalwood.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.73**

*gaurā-gāye aruṇa-vasana ujiyāra\
prātaḥkālera sūrya yini varaṇa tāhāra*

**Jayapatākā Swami:** Lord Gaurāṅga wore saffron garments, the color of
the rising moon in the early morning. He was effulgent like the rising
sun in the early morning.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.74**

*daṇḍa-kare āise prabhu siṁhera gamane\
dekhiyā sakala loka paḍila caraṇe*

**Jayapatākā Swami:** Lord Gaurāṅga carried a *dānḍa* stick and He
approached walking like a lion. Seeing the Lord, all the people fell
before His lotus feet. Everyone was offering Lord Caitanya prostrated
obeisances.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.75**

*hiyā juḍāila dekhi' aṅgera chaṭāka\
pāśarila sarvaloka duḥkha lākhe lākha*

**Jayapatākā Swami:** Seeing the effulgence of His transcendental body,
everyone felt their hearts become filled with cooling bliss. Everyone
forgot their millions of sorrows.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.76**

*premāya bharila hiyā--nāhi śoka duḥkha\
ekadṛṣṭe cāhe śacī viśvambharamukha*

**Jayapatākā Swami:** Every heart was filled with ecstatic love. No one
felt any lamentation or sorrow. With unblinking eyes Mother Śacī gazed
at Lord Viśvambhara\'s face.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.77**

*yateka āchila duḥkha--kichu nāhi cite\
amiyā-siñcila mukha dekhite dekhite*

**Jayapatākā Swami:** Although she had been feeling grief, seeing the
Lord no grief was in her heart. Gazing and gazing at the Lord\'s lotus
face, she felt showered with nectar.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.78**

*advaita-ācārya-gosāñi ānanda-hiyāya\
divyāsane basāila prabhu gaurārāya*

**Jayapatākā Swami:** Lord Advaita Acārya Gosāñi felt that His heart was
filled with transcendental bliss. Lord Gaura Rāya was seated on a
splendid *āsana*.

**Murāri Gupta Kaḍaca 3.4.19:** With the touch His lotus feet the
peerless *guru* beautified the excellent residence of Advaita. Seated on
a fine seat Gaura shone with fierce rays like the sun.

**Murāri Gupta Kaḍaca 3.4.20:** With faltering voice He sang
pastime-songs of Śrī Hari, and as His body glistened with the ornaments
of His tears, He appeared like the sage Nārāyaṇa-deva amidst the *ṛṣis*
at Badarikāśrama.

**Jayapatākā Swami:** Lord Caitanya was sing the pastimes song of Lord
Kṛṣṇa and His voice was trembling in ecstasy and He was Nara-Nārāyaṇa
*ṛṣis* in Badarikāśrama served by various sages and *ṛṣis.*

**Caitanya Carita Mahā Kāvya 11.69:** Gaurāṅga, sitting on an excellent,
pure seat, embraced Advaita, whose body was covered in tears. Along with
the weeping devotees he sang beautiful songs containing the holy name,
which were endowed with great, profound qualities.

**Jayapatākā Swami:** We see how the devotees are expressing their
spontaneous loving sentiments.

**Caitanya-bhāgavata Antya-khaṇḍa 1.208**

*advaitācāryera gaurabhakti---\
sambhrame advaita dekhi\' nija-prāṇa-nātha\
pāda-padme paḍilena hai\' daṇḍapāta*

**Jayapatākā Swami:** Lord Advaita upon seeing the Lord of His life, He
offered prostrated obeisances at the lotus feet of the Lord Caitanya.

**Caitanya-bhāgavata Antya-khaṇḍa 1.209**

*ārta-nāde lāgilena krandana karite\
nā chāḍena pāda-padma dui bāhu haite*

**Jayapatākā Swami:** Lord Advaita then began to cry so forcefully. He
held the lotus feet of the Lord with His two arms and didn't want to
release them.

**Caitanya-bhāgavata Antya-khaṇḍa 1.210**

*śrī-caraṇa-abhiṣeka kari\' prema-jale\
dui haste tuli\' prabhu lailena kole*

**Jayapatākā Swami:** He performed the *abhiṣeka* of the Lord' s lotus
feet with tears of love from His eyes. Lord Caitanya picked Him up with
His two hands and embraced Lord Advaita.

**Caitanya Maṅgala, Madhya-khaṇḍa 14.79**

*pādaprakṣālana kari' muchiyā vasāna\
pādodaka-pāna kaila saba nijajana*

**Jayapatākā Swami:** He washed the Lord\'s lotus feet and then He dried
them with a cloth. All the devotees drank the water which has washed the
Lord\'s feet.

**Caitanya-bhāgavata Antya-khaṇḍa 1.211**

*ācārya bhāsilāṭhākurera prema-jale\
ānande mūrcchita hai\' paḍe pada-tale*

**Jayapatākā Swami:** Advaita Ācārya floated in the transcendental
waters of Lord Gaurāṅga's pure love. Overwhelmed by ecstasy He lost His
consciousness fell at the Lord\'s lotus feet.

**Caitanya-bhāgavata Antya-khaṇḍa 1.212**

*sthira hai\' ṭhākura vasilā kata-kṣaṇe\
uṭhila paramānanda advaita-bhavane*

**Jayapatākā Swami:** After a while Lord Advaita became pacified and sat
down by the Lord, and His entire house became filled with transcendental
ecstasy.

Transcribed by Jayarāseśvarī devī dāsī - 1st October 2020

Verified by JPS Archives team - 1st October 2020
